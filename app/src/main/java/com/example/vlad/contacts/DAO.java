package com.example.vlad.contacts;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vlad on 12/1/2016.
 */

public class DAO extends SQLiteOpenHelper{
    SQLiteDatabase database;
    Context context;

    public DAO(Context context){
        super(context, "Contacts", null, 1);
        this.database = this.getWritableDatabase();
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS contact (contactName VARCHAR, numero VARCHAR)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public List<Contact> getContacts(){
        List<Contact> contacts = new ArrayList<Contact>();
        Cursor cursor = database.rawQuery("SELECT * FROM contact", null);

        while(cursor.moveToNext()){
            Contact contact = new Contact();
            contact.setName(cursor.getString(0));
            contact.setNumero(cursor.getString(1));
            contacts.add(contact);
        }

        return contacts;
    }

    public void addUser(String name, String numero) {
        String[] args = {name, numero};
        database.execSQL("INSERT INTO contact (contactName, numero) VALUES( ?, ? )", args);
    }

}
