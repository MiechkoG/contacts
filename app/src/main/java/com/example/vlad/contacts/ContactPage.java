package com.example.vlad.contacts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Vlad on 12/1/2016.
 */

public class ContactPage extends AppCompatActivity {
    List<Contact> contactList;
    DAO dao;
    int currentContactIndex = 0;
    TextView nomTextView;
    TextView numeroTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contacts_page);

        nomTextView = (TextView)findViewById(R.id.nomTextView);
        numeroTextView = (TextView)findViewById(R.id.numeroTextView);

        dao = new DAO(getApplicationContext());
        contactList = dao.getContacts();
        currentContactIndex = contactList.size();

        if(contactList.size() > 0) {
            Contact c = contactList.get(currentContactIndex-1);
            if (c != null) {
                nomTextView.setText(c.getName());
                numeroTextView.setText(c.getNumero());
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.contact_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       switch (item.getItemId()){
           case R.id.addcontact:
               addUser();
               return true;
           case R.id.previouscontact:
               previousUser();
               return true;
           case R.id.nextcontact:
               nextUser();
               return true;
           default:
               return true;
       }
    }

    private void previousUser(){
        if((currentContactIndex-1) < 0){
            currentContactIndex = contactList.size()-1;
        }else{
            currentContactIndex --;
        }
        updateContent();
    }

    private void addUser(){
        String name = nomTextView.getText().toString();
        String numero = numeroTextView.getText().toString();
        dao.addUser(name, numero);
        contactList = dao.getContacts();
        updateContent();
    }

    private void nextUser(){
        if((currentContactIndex+1) >= contactList.size()){
            currentContactIndex = 0;
        }else{
            currentContactIndex ++;
        }
        updateContent();
    }

    private void updateContent(){
        Contact c = contactList.get(currentContactIndex);
        nomTextView.setText(c.getName());
        numeroTextView.setText(c.getNumero());
    }
}
