package com.example.vlad.contacts;

/**
 * Created by Vlad on 12/1/2016.
 */

public interface PasswordCheckerInterface {
    public void onPasswordEntered(String password);
}
