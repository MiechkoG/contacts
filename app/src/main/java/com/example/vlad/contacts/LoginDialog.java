package com.example.vlad.contacts;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

/**
 * Created by Vlad on 12/1/2016.
 */

public class LoginDialog extends DialogFragment {
    private PasswordCheckerInterface passwordCheckerInterface;
    private View v;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        v = LayoutInflater.from(getActivity()).inflate(R.layout.login_dialog, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Mot de passe : ");
        builder.setView(v);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EditText passwordInput = (EditText) v.findViewById(R.id.passwordInput);
                String password = passwordInput.getText().toString();
                passwordCheckerInterface.onPasswordEntered(password);
            }
        });

        return builder.create();
    }

    public void attach(PasswordCheckerInterface passwordCheckerInterface){
        this.passwordCheckerInterface = passwordCheckerInterface;
    }
}
