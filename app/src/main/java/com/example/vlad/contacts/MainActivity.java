package com.example.vlad.contacts;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity  implements PasswordCheckerInterface{
    private String password = "password";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    protected void showDialogLogin(View v){
        FragmentManager fragmentManager = getSupportFragmentManager();
        LoginDialog dialog = new LoginDialog();
        dialog.attach(this);
        dialog.show(fragmentManager, "DialogLogin");
    }

    @Override
    public void onPasswordEntered(String password) {
        if(this.password.equalsIgnoreCase(password)){
            Intent intent = new Intent(this, ContactPage.class);
            startActivity(intent);
        }else{
            new AlertDialog.Builder(this).setTitle("Mot de passe invalide").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            }).show();
        }
    }
}
