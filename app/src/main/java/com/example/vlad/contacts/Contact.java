package com.example.vlad.contacts;

/**
 * Created by Vlad on 12/1/2016.
 */

public class Contact {
    private String name;
    private String numero;

    public Contact() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
}
